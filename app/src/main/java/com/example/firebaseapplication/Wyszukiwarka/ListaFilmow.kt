package com.example.firebaseapplication.Wyszukiwarka

import com.google.gson.annotations.SerializedName

class ListaFilmow(
    title: String,
    image: String,
    id: String
) {
    @SerializedName("title")
    val title = title

    @SerializedName("image")
    val image = image

    @SerializedName("id")
    val id = id
}