package com.example.firebaseapplication.Wyszukiwarka

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RapidApi {
    @GET("/search/{phrase}")
    fun search(@Path("phrase") phrase: String): Call<Odpowiedz>
}