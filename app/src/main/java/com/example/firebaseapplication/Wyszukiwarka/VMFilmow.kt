package com.example.firebaseapplication.Wyszukiwarka

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class VMFilmow: ViewModel() {

    private var retrofit: Retrofit
    var movies: MutableLiveData<Odpowiedz?> = MutableLiveData()

    init {
        retrofit = RapidClient.getInstace()
    }

    fun search(phrase: String) {

        val getDataService = RapidClient.getInstace().create(RapidApi::class.java)
        val call = getDataService.search(phrase)

        call.enqueue(object: Callback<Odpowiedz> {
            override fun onFailure(call: Call<Odpowiedz>, t: Throwable) {
                println("Nie udalo sie")
                println(t.message)
            }

            override fun onResponse(call: Call<Odpowiedz>, response: Response<Odpowiedz>) {
                println("Udalo sie")
                movies.postValue(response.body())
            }
        })
    }
}