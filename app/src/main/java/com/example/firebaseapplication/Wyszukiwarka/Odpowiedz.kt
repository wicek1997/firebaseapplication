package com.example.firebaseapplication.Wyszukiwarka

import com.example.firebaseapplication.Wyszukiwarka.ListaFilmow
import com.google.gson.annotations.SerializedName

class Odpowiedz(titles: ArrayList<ListaFilmow>) {

    @SerializedName("titles")
    val titles: ArrayList<ListaFilmow> = titles
}