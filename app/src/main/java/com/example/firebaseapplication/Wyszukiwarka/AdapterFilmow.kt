package com.example.firebaseapplication.Wyszukiwarka

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.lista_filmow.view.*

class AdapterFilmow(val items: ArrayList<ListaFilmow>, val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val moviesViewHolder = holder as ViewHolder
        moviesViewHolder.name?.text = items.get(position).title

        Picasso.with(context)
            .load(items.get(position).image)
            .resize(290, 400)
            .into(moviesViewHolder.image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.lista_filmow,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val name = view.movies_title
        val image = view.movies_image
    }
}