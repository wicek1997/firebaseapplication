package com.example.firebaseapplication.Wyszukiwarka

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R

class Fragment_Wyszukiwarka : Fragment() {
    private lateinit var moviesViewmodel: VMFilmow

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // tworzenie widoku
        val view = inflater.inflate(R.layout.fragment_wyszukiwarka, container, false)

        val btnSearch = view.findViewById<Button>(R.id.btn_search)
        val searchPhrase = view.findViewById<EditText>(R.id.search_phrase)
        val searchValidation = view.findViewById<TextView>(R.id.search_validation)

        // Ustawienie recyclerView
        val moviesRecyclerView: RecyclerView = view.findViewById(R.id.movies_list)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        moviesRecyclerView.layoutManager = llm

        moviesViewmodel = ViewModelProviders.of(this).get(VMFilmow::class.java)

        btnSearch.setOnClickListener {
            val searchPhrase: String = searchPhrase.text.toString()

            println(searchPhrase.length)
            if(searchPhrase.length === 0) {
                searchValidation.text = "Wpisz cos"
                false
            } else {
                searchValidation.text = ""
                moviesViewmodel.search(searchPhrase)
            }
        }

        moviesViewmodel.movies.observe(this, Observer{ moviesResponse ->
            if(moviesResponse !== null) {
                println("Nie null")
                moviesRecyclerView.adapter =
                    AdapterFilmow(
                        moviesViewmodel.movies.value!!.titles,
                        context!!
                    )
            } else {
                println("null")
            }
        })
        return view
    }
}