package com.example.firebaseapplication.Ksiazka

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_film.*
import kotlinx.android.synthetic.main.fragment_film.nazwaKsiazkaInput
import kotlinx.android.synthetic.main.fragment_film.statusKsiazkaInput
import kotlinx.android.synthetic.main.fragment_film.typKsiazkaInput
import kotlinx.android.synthetic.main.fragment_ksiazka.*
import kotlinx.android.synthetic.main.ksiazka_row.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Fragment_Ksiazka: Fragment(),AdapterKsiazka.OnKsiazkaDeleteListener{
    private lateinit var myRef: DatabaseReference
    private lateinit var listofItems: ArrayList<KsiazkaRow>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_ksiazka, container, false)
        var recyclerViewKsiazka = rootView.findViewById<View>(R.id.recyclerViewKsiazka) as RecyclerView
        recyclerViewKsiazka.layoutManager = GridLayoutManager(context,1)

        val firebase = FirebaseDatabase.getInstance()
        myRef = firebase.getReference("Ksiazki")
       // val id = myRef.push().key.toString()
      //  myRef.child(id).removeValue()

        listofItems = ArrayList()
        recyclerViewKsiazka.adapter =
            AdapterKsiazka(listofItems,this)

             var submitDataBtn = rootView.findViewById<Button>(R.id.dodajKsiazkaInput)
        submitDataBtn.setOnClickListener {
                val nadawanieDaty = SimpleDateFormat("yyyy-MM-dd HH:mm")
                val id = Date().time.toString()
                val nazwa = nazwaKsiazkaInput.text.toString()
                val nazwisko = nazwiskoKsiazkaInput.text.toString()
                val status = statusKsiazkaInput.selectedItem.toString()
                val typ = typKsiazkaInput.selectedItem.toString()
                val data =  nadawanieDaty.format(Date()).toString()

                val firebaseInput = KsiazkaRow(
                    id,
                    nazwa,
                    nazwisko,
                    status,
                    typ,
                    data
                )
                myRef.child(id).setValue(firebaseInput)
                val toast = Toast.makeText(context, "Poszło", Toast.LENGTH_LONG)
                toast.show()
            }





        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(DatabaseError: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                listofItems = ArrayList()

                for(i in dataSnapshot.children){
                    val newRow = i.getValue(KsiazkaRow::class.java)
                    listofItems.add(newRow!!)
                }
                //(activity as MainActivity).setupAdapter()
                setupAdapter(listofItems)
            }

            private fun setupAdapter(arrayData: ArrayList<KsiazkaRow>) {
                recyclerViewKsiazka.adapter =
                    AdapterKsiazka(arrayData,this@Fragment_Ksiazka)
            }
        })

        return rootView
    }

    override fun onItemClick(item: KsiazkaRow, position: Int) {
        deleteGame(item.wczytajIdKsiazka())
    }

    fun deleteGame(ksiazkaID: String){
        myRef.child(ksiazkaID).removeValue()
        Toast.makeText(context, "Usunięto", Toast.LENGTH_LONG).show()
    }
}

