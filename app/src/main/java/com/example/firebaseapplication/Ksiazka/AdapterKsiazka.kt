package com.example.firebaseapplication.Ksiazka

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R

class AdapterKsiazka (private val dataArray: ArrayList<KsiazkaRow>, var clickListner: OnKsiazkaDeleteListener): RecyclerView.Adapter<AdapterKsiazka.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.ksiazka_row,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataArray.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.nazwaKsiazkaTV.setText(dataArray[holder.adapterPosition].nazwa)
        holder.nazwiskoKsiazkaTV.setText(dataArray[holder.adapterPosition].nazwisko)
        holder.statusKsiazkaTV.setText(dataArray[holder.adapterPosition].status)
        holder.typKsiazkaTV.setText(dataArray[holder.adapterPosition].typ)
        holder.dataKsiazkaTV.setText(dataArray[holder.adapterPosition].data)
        holder.initialize(dataArray.get(position),clickListner)

    }

    inner class MyViewHolder(view: View): RecyclerView.ViewHolder(view){
        val nazwaKsiazkaTV = view.findViewById(R.id.nazwaKsiazkaTV) as TextView
        val nazwiskoKsiazkaTV = view.findViewById(R.id.nazwiskoKsiazkaTV) as TextView
        val statusKsiazkaTV = view.findViewById(R.id.statusKsiazkaTV) as TextView
        val typKsiazkaTV = view.findViewById(R.id.typKsiazkaTV) as TextView
        val dataKsiazkaTV = view.findViewById(R.id.dataKsiazkaTV) as TextView
        fun initialize(item: KsiazkaRow, action: OnKsiazkaDeleteListener){
            itemView.setOnClickListener{
                action.onItemClick(item,adapterPosition)
            }
    }


}
    interface OnKsiazkaDeleteListener{
        fun onItemClick(item: KsiazkaRow, position: Int)
    }
}


