package com.example.firebaseapplication.Gra

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R

class AdapterGra (private val dataArray: ArrayList<GraRow>, var clickListner: OnGameDeleteListener): RecyclerView.Adapter<AdapterGra.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.gra_row,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataArray.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.nazwaGraTV.setText(dataArray[holder.adapterPosition].nazwa)
        holder.nazwiskoGraTV.setText(dataArray[holder.adapterPosition].nazwisko)
        holder.statusGraTV.setText(dataArray[holder.adapterPosition].status)
        holder.typGraTV.setText(dataArray[holder.adapterPosition].typ)
        holder.dataGraTV.setText(dataArray[holder.adapterPosition].data)
        holder.initialize(dataArray.get(position),clickListner)

    }

    inner class MyViewHolder(view: View): RecyclerView.ViewHolder(view){
        val nazwaGraTV = view.findViewById(R.id.nazwaKsiazkaTV) as TextView
        val nazwiskoGraTV = view.findViewById(R.id.nazwiskoKsiazkaTV) as TextView
        val statusGraTV = view.findViewById(R.id.statusKsiazkaTV) as TextView
        val typGraTV = view.findViewById(R.id.typKsiazkaTV) as TextView
        val dataGraTV = view.findViewById(R.id.dataKsiazkaTV) as TextView
        fun initialize(item: GraRow, action: OnGameDeleteListener){
            itemView.setOnClickListener{
                action.onItemClick(item,adapterPosition)
            }
    }

}
    interface OnGameDeleteListener{
        fun onItemClick(item: GraRow, position: Int)
    }
}



