package com.example.firebaseapplication.Gra

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_film.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Fragment_Gra: Fragment(),AdapterGra.OnGameDeleteListener{
    private lateinit var myRef: DatabaseReference
    private lateinit var listofItems: ArrayList<GraRow>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_gra, container, false)
        var recyclerViewFilm = rootView.findViewById<View>(R.id.recyclerViewKsiazka) as RecyclerView
        recyclerViewFilm.layoutManager = GridLayoutManager(context,1)

        val firebase = FirebaseDatabase.getInstance()
        myRef = firebase.getReference("Gry")

        listofItems = ArrayList()
        recyclerViewFilm.adapter =
            AdapterGra(listofItems,this)

             var submitDataBtn = rootView.findViewById<Button>(R.id.dodajKsiazkaInput)
           submitDataBtn.setOnClickListener {
                val nadawanieDaty = SimpleDateFormat("yyyy-MM-dd HH:mm")
               val id = Date().time.toString()
                val nazwa = nazwaKsiazkaInput.text.toString()
                val imie = imieInputKsiazka.text.toString()
                val status = statusKsiazkaInput.selectedItem.toString()
                val typ = typKsiazkaInput.selectedItem.toString()
                val data =  nadawanieDaty.format(Date()).toString()

                val firebaseInput = GraRow(
                    id,
                    nazwa,
                    imie,
                    status,
                    typ,
                    data
                )
                myRef.child(id).setValue(firebaseInput)
                val toast = Toast.makeText(context, "Poszło", Toast.LENGTH_LONG)
                toast.show()
            }


        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(DatabaseError: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                listofItems = ArrayList()

                for(i in dataSnapshot.children){
                    val newRow = i.getValue(GraRow::class.java)
                    listofItems.add(newRow!!)
                }
                //(activity as MainActivity).setupAdapter()
                setupAdapter(listofItems)
            }

            private fun setupAdapter(arrayData: ArrayList<GraRow>) {
                recyclerViewFilm.adapter =
                    AdapterGra(arrayData,this@Fragment_Gra)
            }
        })

        return rootView
    }
    override fun onItemClick(item: GraRow, position: Int) {
        deleteGame(item.wczytajIdGra())
    }

    fun deleteGame(graID: String){
        myRef.child(graID).removeValue()
        Toast.makeText(context, "Usunięto", Toast.LENGTH_LONG).show()
    }
}

