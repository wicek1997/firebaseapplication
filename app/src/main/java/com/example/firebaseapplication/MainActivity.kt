package com.example.firebaseapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.example.firebaseapplication.Film.Fragment_Film
import com.example.firebaseapplication.Gra.Fragment_Gra
import com.example.firebaseapplication.Konto.LogowanieActivity
import com.example.firebaseapplication.Ksiazka.Fragment_Ksiazka
import com.example.firebaseapplication.Wyszukiwarka.Fragment_Wyszukiwarka
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    lateinit var mAuth : FirebaseAuth
    lateinit var mLogoutBtn : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       mAuth = FirebaseAuth.getInstance()
        mLogoutBtn = findViewById(R.id.MainLogoutBtn)

        mLogoutBtn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val startIntent = Intent(applicationContext, LogowanieActivity::class.java)
            startActivity(startIntent)
            finish()
        }


            val fm = supportFragmentManager
            val fragmentFilm = Fragment_Film()
            val fragmentGra = Fragment_Gra()
            val fragmentKsiazka = Fragment_Ksiazka()

            val fragmentWyszukiwarka = Fragment_Wyszukiwarka()
            fm.beginTransaction().add(R.id.container, fragmentFilm).commit()

            val nav_wiew = findViewById<NavigationView>(R.id.nav_view)
            nav_wiew.setNavigationItemSelectedListener { menuItem ->

                if (menuItem.itemId == R.id.wyloguj) {

                    Toast.makeText(applicationContext, "Wylogowano", Toast.LENGTH_SHORT).show()
                }
                if (menuItem.itemId == R.id.film) {
                    fm.beginTransaction().replace(R.id.container, fragmentFilm).commit()
                    Toast.makeText(applicationContext, "Film", Toast.LENGTH_SHORT).show()
                }

                if (menuItem.itemId == R.id.ksiazka) {
                    fm.beginTransaction().replace(R.id.container, fragmentKsiazka).commit()
                    Toast.makeText(applicationContext, "ksiazka", Toast.LENGTH_SHORT).show()
                }
                if (menuItem.itemId == R.id.gra) {
                    fm.beginTransaction().replace(R.id.container, fragmentGra).commit()
                    Toast.makeText(applicationContext, "gra", Toast.LENGTH_SHORT).show()
                }
                if (menuItem.itemId == R.id.wyszukiwarka) {
                    fm.beginTransaction().replace(R.id.container, fragmentWyszukiwarka).commit()
                    Toast.makeText(applicationContext, "Wyszukiwarka", Toast.LENGTH_SHORT).show()
                }
                true
            }
        }
    }
