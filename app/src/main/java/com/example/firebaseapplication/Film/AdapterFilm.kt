package com.example.firebaseapplication.Film

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R

class AdapterFilm (private val dataArray: ArrayList<FilmRow>,var clickListner: OnMovieDeleteListener): RecyclerView.Adapter<AdapterFilm.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.film_row,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataArray.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.nazwaTV.setText(dataArray[holder.adapterPosition].nazwa)
        holder.imieTV.setText(dataArray[holder.adapterPosition].imie)
        holder.statusTV.setText(dataArray[holder.adapterPosition].status)
        holder.typTV.setText(dataArray[holder.adapterPosition].typ)
        holder.dataTV.setText(dataArray[holder.adapterPosition].data)
        holder.initialize(dataArray.get(position),clickListner)

    }

    inner class MyViewHolder(view: View): RecyclerView.ViewHolder(view){
        val nazwaTV = view.findViewById(R.id.nazwaKsiazkaTV) as TextView
        val imieTV = view.findViewById(R.id.nazwiskoKsiazkaTV) as TextView
        val statusTV = view.findViewById(R.id.statusKsiazkaTV) as TextView
        val typTV = view.findViewById(R.id.typKsiazkaTV) as TextView
        val dataTV = view.findViewById(R.id.dataKsiazkaTV) as TextView

        fun initialize(item: FilmRow, action: OnMovieDeleteListener){
            itemView.setOnClickListener{
                action.onItemClick(item,adapterPosition)
            }
        }
    }
    interface OnMovieDeleteListener{
        fun onItemClick(item: FilmRow, position: Int)
    }

}



