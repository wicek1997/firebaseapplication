package com.example.firebaseapplication.Film

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapplication.R
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_film.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Fragment_Film: Fragment(), AdapterFilm.OnMovieDeleteListener {
    private lateinit var myRef: DatabaseReference
    private lateinit var listofItems: ArrayList<FilmRow>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_film, container, false)
        var recyclerViewFilm = rootView.findViewById<View>(R.id.recyclerViewKsiazka) as RecyclerView
        recyclerViewFilm.layoutManager = GridLayoutManager(context,1)

        val firebase = FirebaseDatabase.getInstance()
        myRef = firebase.getReference("Filmy")
       // val id = myRef.push().key.toString()
      //  myRef.child(id).removeValue()

        listofItems = ArrayList()
        recyclerViewFilm.adapter =
            AdapterFilm(listofItems,this)

             var submitDataBtn = rootView.findViewById<Button>(R.id.dodajKsiazkaInput)
           submitDataBtn.setOnClickListener {
                val nadawanieDaty = SimpleDateFormat("yyyy-MM-dd HH:mm")
                val id = Date().time.toString()
                val nazwa = nazwaKsiazkaInput.text.toString()
                val imie = imieInputKsiazka.text.toString()
                val status = statusKsiazkaInput.selectedItem.toString()
                val typ = typKsiazkaInput.selectedItem.toString()
                val data =  nadawanieDaty.format(Date()).toString()

                val firebaseInput = FilmRow(
                    id,
                    nazwa,
                    imie,
                    status,
                    typ,
                    data
                )
                myRef.child(id).setValue(firebaseInput)
                val toast = Toast.makeText(context, "Poszło", Toast.LENGTH_LONG)
                toast.show()
            }





        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(DatabaseError: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                listofItems = ArrayList()

                for(i in dataSnapshot.children){
                    val newRow = i.getValue(FilmRow::class.java)
                    listofItems.add(newRow!!)
                }
                //(activity as MainActivity).setupAdapter()
                setupAdapter(listofItems)
            }

            private fun setupAdapter(arrayData: ArrayList<FilmRow>) {
                recyclerViewFilm.adapter =
                    AdapterFilm(arrayData,this@Fragment_Film)
            }
        })

        return rootView
    }

    override fun onItemClick(item: FilmRow, position: Int) {
        usunFilm(item.zwrocIdFilm())
    }

    fun usunFilm(idFilm: String){
        myRef.child(idFilm).removeValue()
        Toast.makeText(context, "Usunięto", Toast.LENGTH_LONG).show()

    }


}

