package com.example.firebaseapplication.Film

    class FilmRow(
    val idFilm: String = "",
    val nazwa: String = "",
    val imie: String ="",
    val status: String = "",
    val typ: String ="",
    val data: String = ""
)
{
    fun zwrocIdFilm(): String{
        return idFilm
    }
}